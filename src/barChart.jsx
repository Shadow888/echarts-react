/* eslint semi:1,prettier/prettier:0,one-var:0,no-unused-vars:0,yoda:0,quotes:1 */

import React from "react";
import Hammer from "react-hammerjs";
import { baseChartProps } from "./public/baseChart.js";
import BaseRel from "./public/baseRel.jsx";
import { barFunnelProps } from "./barFunnelChart/barFunnelChart.jsx";
import BarFunnelChart from "./barFunnelChart/barFunnelChart.jsx";

export default class RelBarChart extends BaseRel {
  getStyle() {
    var ret = {
      flexShrink: 0
    };
    for (var i of ["width", "height", "backgroundColor"]) {
      if (this.props[i]) {
        ret[i] = this.props[i];
      }
    }
    return Object.assign({}, this.baseStyle(), this.relStyle(), ret);
  }
  render() {
    return this.props.visible ? (
      <BarFunnelChart
        {...this.props}
        render={({ chartRef }) => (
          <Hammer
            onTap={this.onTap}
            onDoubleTap={this.onDblTap}
            onPress={this.onLongpress}
            onSwipe={this.onSwipe}
            onSwipeLeft={this.onSwipeLeft}
            onSwipeRight={this.onSwipeRight}
            onSwipeUp={this.onSwipeUp}
            onSwipeDown={this.onSwipeDown}
          >
            <div style={this.getStyle()} onClick={this.emitClick}>
              <div
                ref={chartRef}
                style={{
                  width: "100%",
                  height: "500px"
                }}
              />
            </div>
          </Hammer>
        )}
      />
    ) : null;
  }
}

RelBarChart.defaultProps = {
  ...BaseRel.defaultProps,
  ...baseChartProps,
  ...barFunnelProps
};
