export function colToRow(arr, colNum, defaultName = "未知分组") {
  return arr.map(row => (colNum < row.length ? row[colNum] : defaultName));
}
const noop = () => {};

export const dstrtDate = function(dateObj) {
  return {
    year: dateObj.getFullYear(),
    month: dateObj.getMonth() + 1,
    date: dateObj.getDate()
  };
};
export function toNumber(numberLike) {
  return isNaN(numberLike) ? 0 : +numberLike;
}

export function pad(str, padWith = "0", leastLen = 2) {
  return (
    Array.from({
      length: leastLen
    })
      .fill(padWith)
      .join("") +
    ("" + str)
  ).slice(-leastLen);
}

function leaveUniqueValue(arr) {
  const set = new Set(arr);
  return Array.from(set);

}
const seriesDefault = "未知系列";

function arrayToChartSeries(arr) {
  if (!Array.isArray(arr)) return noop;
  const rowNum = arr.length;
  if (rowNum === 0) return noop;
  if (!(arr[0] instanceof Array)) arr = [arr];
  const colNum = arr[0].length;
  let context = {};
  if (colNum < 2) {
    return noop;
  } else if (colNum === 2) {
    context.xAxisData = leaveUniqueValue(colToRow(arr, 0));
    context.xAxisDataWithIndex = context.xAxisData.reduce((res, t, i) => {
      res[t] = i;
      return res;
    }, {});
    context.series = [
      {
        name: seriesDefault,
        data: arr.reduce((res, r) => {
          res[context.xAxisDataWithIndex[r[0]]] = toNumber(r[1]);
          return res;
        }, new Array(context.xAxisData.length))
      }
    ];
    return context;
  } else {
    context.xAxisData = leaveUniqueValue(colToRow(arr, 1));
    context.xAxisDataWithIndex = context.xAxisData.reduce((res, t, i) => {
      res[t] = i;
      return res;
    }, {});
    context.series = arr.reduce((res, r) => {
      const key = r[0] || seriesDefault;
      if (!res[key]) res[key] = new Array(context.xAxisData.length).fill(0);
      res[key][context.xAxisDataWithIndex[r[1]]] = toNumber(r[2]);
      return res;
    }, {});
    context.series = Object.keys(context.series).map(s => ({
      name: s,
      data: context.series[s]
    }));
    return context;
  }
}

export function arrayToScatterChartSeries(arr) {    //
  if (!Array.isArray(arr)) return noop;
  const rowNum = arr.length;
  if (rowNum === 0) return noop;
  if (!(arr[0] instanceof Array)) arr = [arr];
  const colNum = arr[0].length;
  if (colNum < 3) {
    return noop;
  } else {
    return arr.reduce((res, r) => {
      const key = r[0] || seriesDefault;
      if (!res[key]) res[key] = [];
      res[key].push(r.slice(1));
      return res;
    }, {});
  }
}
export function arrayToGanttChartSeries(arr) {
  if (!Array.isArray(arr)) return noop;
  const rowNum = arr.length;
  if (rowNum === 0) return noop;
  if (!(arr[0] instanceof Array)) arr = [arr];
  const colNum = arr[0].length;
  if (colNum < 3) {
    return noop;
  } else {
    return {
      series: Array.from(
        arr.reduce((res, r) => {
          res.add(r[0]);
          return res;
        }, new Set())
      ),
      data: arr
    };
  }
}
export default arrayToChartSeries;
