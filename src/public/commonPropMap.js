/* eslint-disable prettier/prettier */
import propType from "./propType";

export default {
  showXAxisSplitLine: {
    name: "showXAxisSplitLine",
    type: propType.Boolean,
    defaultValue: false,
    locale: {
      en: "Show X Axis Split Line",
      zh: "X轴分割线"
    }
  },
  showYAxisSplitLine: {
    name: "showYAxisSplitLine",
    type: propType.Boolean,
    defaultValue: false,
    locale: {
      en: "Show Y Axis Split Line",
      zh: "Y轴分割线"
    }
  },
  showDataZoom: {
    name: "showDataZoom",
    type: propType.Boolean,
    locale: {
      en: "dataZoom",
      zh: "数据可缩放"
    },
    defaultValue: false
  }
};
