import React from "react";
import Ih5Base, { baseDefaultProps } from "./base.jsx";

export default class Ih5RelBase extends Ih5Base {
  constructor(props) {
    super(props);
    this.state = {};

    this.relStyle = this.relStyle.bind(this);
  }
  relStyle() {
    let obj = {};
    if (
      !(
        this.props.marginTop === 0 &&
        this.props.marginRight === 0 &&
        this.props.marginBottom === 0 &&
        this.props.marginLeft === 0
      )
    ) {
      obj.margin = `${this.props.marginTop}px ${this.props.marginRight}px ${
        this.props.marginBottom
      }px ${this.props.marginLeft}px`;
    }
    if (this.props.maxHeight) {
      obj["maxHeight"] = this.props.maxHeight;
    }
    if (this.props.minHeight) {
      obj["minHeight"] = this.props.minHeight;
    }
    if (this.props.maxWidth) {
      obj["maxWidth"] = this.props.maxWidth;
    }
    if (this.props.minWidth) {
      obj["minWidth"] = this.props.minWidth;
    }
    if (this.props.opacity !== undefined && this.props.opacity !== 1) {
      obj.opacity = this.props.opacity;
    }
    if (!this.props.visible) {
      obj["display"] = "none";
    }
    if (this.props.transitionPro) {
      obj["transitionProperty"] = this.props.transitionPro;
    }
    if (this.props.transitionTime) {
      obj["transitionDuration"] = this.props.transitionTime + "ms";
    }
    if (this.props.transitionType) {
      obj["transitionTimingFunction"] = this.props.transitionType;
    }
    if (this.props.bgColor) {
      obj["backgroundColor"] = this.props.bgColor;
    }
    return obj;
  }
  render() {
    return <div className="ih5-base-rel" />;
  }
}

let defaultProps = {
  width: "",
  height: "",
  visible: true,
  opacity: 1,
  marginTop: 0,
  marginRight: 0,
  marginBottom: 0,
  marginLeft: 0
};

let relBaseDefaultProps = Object.assign({}, baseDefaultProps, defaultProps);

Ih5RelBase.defaultProps = relBaseDefaultProps;

export { relBaseDefaultProps };
