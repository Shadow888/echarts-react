/* eslint-disable prettier/prettier,one-var */

export function getTooltip(tooltipTrigger) {
  return {
    trigger: tooltipTrigger
  };
}

export function getLegend(color, series) {
  return {
    left: 'left',
    textStyle: {
      color
    },
    data: series.map(s => s.name)
  };
}

export function getToolbox(allowSaving, title) {
  return {
    right: "20%",
    feature: {
      saveAsImage: allowSaving
        ? {
            name: title
          }
        : undefined
    }
  };
}

function getAxisLine(color) {
  return {
    lineStyle: {
      color
    }
  };
}

function getSplitLine(show) {
  return { show };
}

export function getXAxis(xAxisData, color, showXAxisSplitLine) {
  return {
    data: xAxisData,
    axisLine: getAxisLine(color),
    splitLine: getSplitLine(showXAxisSplitLine)
  };
}

export function getYAxis(color, showYAxisSplitLine) {
  return {
    axisLine: getAxisLine(color),
    splitLine: getSplitLine(showYAxisSplitLine)
  };
}

export function getVisualMap(dataValues, range) {
  return {
    show: false,
    min: Math.min.apply(Math, dataValues) / 2,
    max: Math.max.apply(Math, dataValues),
    inRange: {
      colorLightness: range
    }
  };
}
