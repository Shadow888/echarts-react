/* eslint-disable prettier/prettier */
import propType from "./propType";

export const commonChartProps = {
  content: [
    {
      name: "width",
      type: propType.PercentPx,
      locale: {
        en: "Width",
        zh: "宽度"
      },
      defaultValue: "100%",
      placeholder: {
        en: "Auto",
        zh: "自动(原始宽度)"
      }
    },
    {
      name: "height",
      defaultValue: "100%",
      type: propType.PercentPx,
      locale: {
        en: "Height",
        zh: "高度"
      },
      placeholder: {
        en: "Auto",
        zh: "自动(原始高度)"
      }
    },
    {
      name: "bgColor",
      type: propType.Color,
      defaultValue: "",
      locale: {
        en: "BackgroundColor",
        zh: "背景颜色"
      }
    },
    {
      name: "title",
      type: propType.String,
      defaultValue: "",
      locale: {
        en: "title",
        zh: "标题"
      }
    },
    {
      name: "allowSaving",
      defaultValue: false,
      type: propType.Boolean,
      locale: {
        en: "allowSaving",
        zh: "允许保存图片"
      }
    },
    {
      name: "fontColor",
      defaultValue: "#333333",
      type: propType.Color,
      locale: {
        en: "fontColor",
        zh: "文字颜色"
      }
    },
    /*{
      name: 'colorArr',
      eventHidden: false,
      type: propType.Hidden,
      locale: {
          en: 'color data',
          zh: '颜色数据'
      },
      get defaultValue() {
          return ['#C23531', '#2F4554', '#61A0A8', '#D48265', '#91C7AE', '#749F83', '#CA8622', '#BDA29A', '#6E7074', '#546570', '#C4CCD3']
      }
  }, {
      name: 'dataArr',
      eventHidden: false,
      type: propType.Hidden,
      locale: {
          en: 'data',
          zh: '数据'
      },
      get defaultValue() {
          return []
      }
  },*/
    {
      name: "colorArrHeaders",
      type: propType.Hidden,
      get defaultValue() {
        return Array.from({
          length: 11
        }).map(() => ({
          type: "c",
          title: ""
        }));
      }
    },
    {
      name: "dataArrHeaders",
      type: propType.Hidden,
      get defaultValue() {
        return [
          {
            type: "s",
            title: "系列"
          },
          {
            type: "s",
            title: "类目"
          },
          {
            type: "s",
            title: "数据"
          }
        ];
      }
    },
    {
      name: "dataArr",
      type: propType.VbindButton,
      locale: {
        en: "edit chart data",
        zh: "编辑图表数据"
      },
      extraConfig: {
        action: "togglePropDataEditor",
        headersPropName: "dataArrHeaders",
        arrType: "two"
      },
      get defaultValue() {
        return [];
      }
    },
    {
      name: "colorArr",
      type: propType.VbindButton,
      locale: {
        en: "edit series color",
        zh: "编辑颜色数据"
      },
      extraConfig: {
        action: "togglePropDataEditor",
        headersPropName: "colorArrHeaders",
        arrType: "one"
      },
      get defaultValue() {
        return [
          "#C23531",
          "#2F4554",
          "#61A0A8",
          "#D48265",
          "#91C7AE",
          "#749F83",
          "#CA8622",
          "#BDA29A",
          "#6E7074",
          "#546570",
          "#C4CCD3"
        ];
      }
    }
  ],
  events: [
    {
      name: "clickonanyarea",
      locale: {
        en: "ClickOnAnyDataPoint",
        zh: "点中任意数据点"
      },
      params: [
        {
          name: "valueOnClick",
          type: propType.Formula,
          locale: {
            en: "value on click",
            zh: "选中数值"
          }
        },
        {
          name: "nameOnClick",
          type: propType.Formula,
          locale: {
            en: "category on click",
            zh: "选中类目"
          }
        },
        {
          name: "seriesOnClick",
          type: propType.Formula,
          locale: {
            en: "series on click",
            zh: "选中系列"
          }
        }
      ]
    }
  ]
};
