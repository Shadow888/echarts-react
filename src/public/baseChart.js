/* eslint semi:1,prettier/prettier:0,one-var:0,no-unused-vars:0,yoda:0,quotes:1 */

import Echarts from "echarts";
import React from "react";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.chartRef = React.createRef();
    this.chart = null;
    this.waitingForRendering = false;
    this.enterRenderPhase = this.enterRenderPhase.bind(this);
  }
  componentDidMount() {
    this.chart = Echarts.init(this.chartRef.current);
    if (this.props.resetChart) {
      console.log("Going to reset chart");
      this.props.resetChart(this.enterRenderPhase);
    }
  }
  componentDidUpdate() {
    if (this.chart && this.props.resetChart) {
      this.props.resetChart(this.enterRenderPhase);
    }
  }
  enterRenderPhase(option) {
    console.log("entering render phase");
    this.waitingForRendering = true;
    setTimeout(() => {
      this.chart.setOption(option, {
        notMerge: true
      });
      this.chart.resize();
      this.waitingForRendering = false;
    }, 100);
  }
  render() {
    if (this.props.render) {
      return this.props.render({
        chartRef: this.chartRef
      });
    }
    return null;
  }
}

export var baseChartProps = {
  width: "100%",
  height: "100%",
  fontColor: "#333333",
  title: "",
  bgColor: "",
  dataArr: [],
  dataArrHeaders: [
    {
      type: "s",
      title: "系列"
    },
    {
      type: "s",
      title: "类目"
    },
    {
      type: "s",
      title: "数据"
    }
  ],
  tooltipTrigger: "item",
  type: "line",
  colorArr: [
    "#C23531",
    "#2F4554",
    "#61A0A8",
    "#D48265",
    "#91C7AE",
    "#749F83",
    "#CA8622",
    "#BDA29A",
    "#6E7074",
    "#546570",
    "#C4CCD3"
  ],
  colorArrHeaders: Array.from({
    length: 11
  }).map(() => ({
    type: "c",
    title: ""
  })),
  allowSaving: false
};
