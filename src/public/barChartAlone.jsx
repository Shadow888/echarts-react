/* eslint semi:1,prettier/prettier:0,one-var:0,no-unused-vars:0,yoda:0,quotes:1 */

import React from "react";
import BaseChart from "./baseChart.js";
import {
  getTooltip,
  getLegend,
  getToolbox,
  getVisualMap,
  getXAxis,
  getYAxis
} from "./barChart.js";
import arrayToChartSeries from "./arrayToChartSeries";



export default class extends React.Component {  //React.createClass写法废弃
  constructor(props) {
    super(props); //没有super，则在子组件中无法通过this.props访问到父组件传递的值
    this.resetChart = this.resetChart.bind(this); //es6写法，确保resetChart调用时，this指代是当前组件
  }
  resetChart(enterRenderPhase) {
    var dataValues = this.props.dataArr.map(x => +x[2]),
      prevXAxis;
    const { xAxisData, series } = this.srcData();
    if (!xAxisData || !series) return;
    const option = {
      tooltip: getTooltip(this.props.tooltipTrigger),
      legend: {
        left: 100,
        textStyle: {
          color: this.props.fontColor
        },
        data: series.map(s => s.name)
      },
      color: this.props.colorArr,
      toolbox: getToolbox(this.props.allowSaving, this.props.title),
      angleAxis: {},
      radiusAxis: {
        type: "category",
        data: xAxisData,
        z: 10
      },
      polar: {},
      series: series,
      dataZoom: {
                type: "slider",
                show: this.props.showDataZoom
            }
    };
    prevXAxis = option.xAxis;
        if (this.props.horizontal) {
            option.xAxis = { ...option.yAxis, type: "value" };
            option.yAxis = { ...prevXAxis, type: "category" };
        }
        if (this.props.gradient) {
            option.visualMap = getVisualMap(dataValues, [0.25, 0.8]);
        }
        console.log("Going to enter render phase");
    enterRenderPhase(option);
  }
  srcData() {
    let chartData = this.props.dataArr;
    let computedData = arrayToChartSeries(chartData);
    let stackName = "stack" + +new Date();
    if (computedData.series) {
      return {
        ...computedData,
        series: computedData.series.map(series => {
          var s = {
            name: series.name,
            type: this.props.type,
            data: series.data.map((item, i) => {
              var ret = {
                name: computedData.xAxisData[i],
                value: item
              };
              if (this.props.colorByData && !this.props.gradient) {
                ret.itemStyle = {
                  color: this.props.colorArr[i % this.props.colorArr.length]
                };
              }
              return ret;
            }),
            label: {
              color: this.props.fontColor
            },
            labelLine: {
              lineStyle: {
                color: this.props.fontColor
              }
            },
            barGap: this.props.overlap ? "-100%" : this.props.barGap * 100 + "%"
          };
          if (this.props.useFixedBarWidth) {
            s.barWidth = this.props.barWidth;
          } else {
            s.barCategoryGap = this.props.barCategoryGap * 100 + "%";
          }
          if (this.props.stack) {
            s.stack = stackName;
          }
          return s;
        })
      };
    }
    return computedData;
  }
  render() {
    return (
      <BaseChart resetChart={this.resetChart} render={this.props.render} />
    );
  }
}
