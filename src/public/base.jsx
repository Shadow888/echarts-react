import React, { Component } from "react";

import isPc from "./isPc";

export default class Ih5Base extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.baseStyle = this.baseStyle.bind(this);
    this.globalStyle = this.globalStyle.bind(this);
    this.emitMouseOver = this.emitMouseOver.bind(this);
    this.emitMouseOut = this.emitMouseOut.bind(this);
    this.emitMouseEnter = this.emitMouseEnter.bind(this);
    this.emitMouseLeave = this.emitMouseLeave.bind(this);
    this.emitClick = this.emitClick.bind(this);
    this.onTap = this.onTap.bind(this);
    this.onSwipe = this.onSwipe.bind(this);
    this.onSwipeLeft = this.onSwipeLeft.bind(this);
    this.onSwipeRight = this.onSwipeRight.bind(this);
    this.onSwipeUp = this.onSwipeUp.bind(this);
    this.onSwipeDown = this.onSwipeDown.bind(this);
    this.onLongpress = this.onLongpress.bind(this);
    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.onTouchStart = this.onTouchStart.bind(this);
    this.onTouchMove = this.onTouchMove.bind(this);
    this.onTouchEnd = this.onTouchEnd.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.onDblTap = this.onDblTap.bind(this);
    this.onContextMenu = this.onContextMenu.bind(this);
    this.onMouseWheel = this.onMouseWheel.bind(this);
  }
  componentDidMount() {
    if (this.props._onInitialize) {
      this.props._onInitialize({});
    }
  }
  className() {
    const { customClass } = this.props;
    if (!customClass) return [];
    return customClass.split(",");
  }
  baseStyle() {
    let obj = {
      cursor: this.props.cursor
    };
    if (
      this.props.borderRadius &&
      this.props.borderRadius !== "0px" &&
      this.props.borderRadius !== "0%"
    ) {
      obj.borderRadius = this.props.borderRadius;
    }
    if (this.props.borderWidth) {
      if (isNaN(this.props.borderWidth)) {
        obj.border = `0 ${this.props.borderStyle} ${this.props.borderColor}`;
        obj["borderWidth"] = this.props.borderWidth;
      } else {
        obj.border = `${this.props.borderWidth}px ${this.props.borderStyle} ${
          this.props.borderColor
        }`;
      }
    }
    Object.assign(obj, this.globalStyle());
    return obj;
  }
  globalStyle() {
    let obj = {};
    let dropShadow = "";
    let blur = "";
    let brightness = "";
    let contrast = "";
    let grayScale = "";
    let hueRotate = "";
    let invert = "";
    let saturate = "";

    if (
      this.props.dropShadowColor &&
      this.props.dropShadowColor !== "transparent"
    ) {
      dropShadow = `drop-shadow(
                ${this.props.dropShadowOffX}px
                ${this.props.dropShadowOffY}px
                ${this.props.dropShadowBlur}px
                ${this.props.dropShadowColor}) `;
    }
    if (this.props.filterBlur) {
      blur = `blur(${this.props.filterBlur}px) `;
    }
    if (this.props.brightness !== undefined && this.props.brightness !== 1) {
      brightness = `brightness(${this.props.brightness}) `;
    }
    if (this.props.contrast !== undefined && this.props.contrast !== 1) {
      contrast = `contrast(${this.props.contrast}) `;
    }
    if (this.props.grayScale) {
      grayScale = `grayscale(${this.props.grayScale}) `;
    }
    if (this.props.hueRotate) {
      hueRotate = `hue-rotate(${this.props.hueRotate}deg) `;
    }
    if (this.props.invert) {
      invert = `invert(${this.props.invert}) `;
    }
    if (this.props.saturate !== undefined && this.props.saturate !== 1) {
      saturate = `saturate(${this.props.saturate}) `;
    }
    if (
      dropShadow ||
      blur ||
      brightness ||
      contrast ||
      grayScale ||
      hueRotate ||
      invert ||
      saturate
    ) {
      obj["filter"] =
        dropShadow +
        blur +
        brightness +
        contrast +
        grayScale +
        hueRotate +
        invert +
        saturate;
    }
    if (!(this.props.originX === 0 && this.props.originY === 0)) {
      obj.transformOrigin = `${this.props.originX * 100}% ${this.props.originY *
        100}% 0`;
    }
    if (this.props.rotate) {
      obj["transform"] = `rotate(${this.props.rotate}deg)`;
    }
    obj["position"] = "relative";
    return obj;
  }
  emitMouseOver(e) {
    if (this.props._onMouseover) {
      this.props._onMouseover(e);
    }
    // this.$emit('mouseover', e)
  }
  emitMouseOut(e) {
    // this.$emit('mouseout', e)
    if (this.props._onMouseout) {
      this.props._onMouseout(e);
    }
  }
  emitMouseEnter(e) {
    // this.$emit('mouseenter', e)
    if (this.props._onMouseenter) {
      this.props._onMouseenter(e);
    }
  }
  emitMouseLeave(e) {
    // this.$emit('mouseleave', e)
    if (this.props._onMouseleave) {
      this.props._onMouseleave({});
    }
  }
  emitClick(e) {
    // this.$emit('click', e)
    if (this.props._onClick) {
      this.props._onClick(e);
    }
  }
  onTap(e) {
    // this.$emit('tap', e)
    if (this.props._onTap) {
      this.props._onTap(e);
    }
  }
  onDblTap(e) {
    if (this.props._onDbltap) {
      this.props._onDbltap(e);
    }
  }
  onContextMenu(e) {
    if (this.props._onContextmenu) {
      e.preventDefault();
      this.props._onContextmenu(e);
    }
  }
  onMouseWheel(e) {
    if (e.deltaY < 0) {
      if (this.props._onMousewheelup) {
        this.props._onMousewheelup(e);
      }
    } else {
      if (this.props._onMousewheeldown) {
        this.props._onMousewheeldown(e);
      }
    }
  }
  onSwipe(e) {
    // this.$emit('touchmove', e)
    if (this.props._onTouchmove) {
      if (e && e.touches) {
        this.props._onTouchmove(e.touches[0]);
      } else if (e) {
        this.props._onTouchmove({ pageX: e.center.x, pageY: e.center.y });
      }
    }
  }
  onSwipeLeft(e) {
    // this.$emit('swipeleft', e)
    if (this.props._onSwipeleft) {
      this.props._onSwipeleft(e);
    }
  }
  onSwipeRight(e) {
    // this.$emit('swiperight', e)
    if (this.props._onSwiperight) {
      this.props._onSwiperight(e);
    }
  }
  onSwipeUp(e) {
    // this.$emit('swipeup', e)
    if (this.props._onSwipeup) {
      this.props._onSwipeup(e);
    }
  }
  onSwipeDown(e) {
    // this.$emit('swipedown', e)
    if (this.props._onSwipedown) {
      this.props._onSwipedown(e);
    }
  }
  onLongpress(e) {
    // this.$emit('longpress', e)
    if (this.props._onLongpress) {
      this.props._onLongpress(e);
    }
  }
  onMouseDown(e) {
    if (isPc()) {
      document.addEventListener("mousemove", this.handleMouseMove);
      document.addEventListener("mouseup", this.handleMouseUp);
      // this.$emit('touchstart', e)
      if (this.props._onTouchstart) {
        this.props._onTouchstart(e);
      }
    }
  }
  onMouseUp(e) {
    // this.$emit('touchend', e)
    if (isPc()) {
      if (this.props._onTouchend) {
        this.props._onTouchend(e);
      }
    }
  }
  onTouchStart(e) {
    // this.$emit('touchstart', e)
    if (this.props._onTouchstart) {
      this.props._onTouchstart(e.touches[0]);
    }
  }
  onTouchMove(e) {
    // this.$emit('mousemove', e)
    if (this.props._onTouchmove) {
      this.props._onTouchmove(e.touches[0]);
    }
  }
  onTouchEnd(e) {
    // this.$emit('touchend', e)
    if (this.props._onTouchend) {
      this.props._onTouchend(e.changedTouches[0]);
    }
  }
  handleMouseMove(e) {
    // this.$emit('mousemove', e)
    if (this.props._onMousemove) {
      this.props._onMousemove(e);
    }
  }
  handleMouseUp() {
    document.removeEventListener("mousemove", this.handleMouseMove);
    document.removeEventListener("mouseup", this.handleMouseUp);
  }
  render() {
    return <div className="ih5-base" />;
  }
}

let baseDefaultProps = {
  borderWidth: 0,
  borderStyle: "solid",
  borderColor: "",
  borderRadius: "0px",
  dropShadowOffX: 0,
  dropShadowOffY: 0,
  dropShadowBlur: 0,
  dropShadowColor: "",
  originX: 0,
  originY: 0,
  rotate: 0,
  cursor: "inherit",
  brightness: 1,
  contrast: 1,
  grayScale: 0,
  hueRotate: 0,
  invert: 0,
  saturate: 1
};

Ih5Base.defaultProps = baseDefaultProps;

export { baseDefaultProps };
