export default {
  String: "String",
  Number: "Number",
  Color: "Color",
  Boolean: "Boolean",
  Text: "Text", // textarea
  PercentPx: "PercentPx",
  Percent: "Percent",
  Degree: "Degree", // 度

  Select: "Select", // 普通选择
  ObjSelect: "ObjSelect", // 对象选择
  VbindSelect: "VbindSelect", // 绑定数据选择
  IconSelect: "IconSelect", // svg图形选择
  ObjResult: "ObjResult", // 输出到对象的选择
  MultiSelect: "MultiSelect", // 多选面板
  DatePicker: "DatePicker", // 日期选择

  SrcUpload: "SrcUpload",
  BtnSwitch: "BtnSwitch",
  SelectMore: "SelectMore",
  Condition: "Condition",
  ServiceParams: "ServiceParams", // 服务参数
  ApiParams: "ApiParams", // api参数

  VbindButton: "VbindButton", // Support panel editing and data binding
  Button: "Button",
  Hidden: "Hidden",

  // 以下类型为事件相关类型
  Formula: "Formula",
  FormulaColor: "FormulaColor",
  ObjSelectCb: "ObjSelectCb", // 对象选择回调
  ObjJsonParamsSelect: "ObjJsonParamsSelect", // 通用变量路径组件

  /* --db相关-- */
  DbCons: "DbCons", // 筛选条件
  DbOrders: "DbOrders", // 排序方式
  DbRange: "DbRange", // 数据范围
  DbUpdates: "DbUpdate", // 数据更新
  DbNumCol: "DbNumCol" // 数值字段
};
