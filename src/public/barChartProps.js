/* eslint semi:1,prettier/prettier:0,one-var:0,no-unused-vars:0,yoda:0,quotes:1 */

import propType from "./propType.js";
import commonPropMap from "./commonPropMap.js";
import { commonChartProps } from "./commonChartProps.js";

export var propMap = {
  ...commonPropMap,
  dataArr: {
    name: "dataArr",
    type: propType.VbindButton,
    locale: {
      en: "edit chart data",
      zh: "编辑图表数据"
    },
    extraConfig: {
      action: "togglePropDataEditor",
      headersPropName: "dataArrHeaders",
      arrType: "two"
    },
    get defaultValue() {
      return [
        ["营销数据", "访问", 60],
        ["营销数据", "咨询", 40],
        ["营销数据", "订单", 20],
        ["营销数据", "点击", 80],
        ["营销数据", "展现", 100]
      ];
    }
  },
  barCategoryGap: {
    name: "barCategoryGap",
    type: propType.Percent,
    defaultValue: 0.2,
    locale: { en: "Bar category gap", zh: "图表分类间隔" }
  },
  gradient: {
    name: "gradient",
    type: propType.Boolean,
    defaultValue: false,
    locale: { en: "Gradient", zh: "使用渐变" }
  },
  colorByData: {
    name: "colorByData",
    type: propType.Boolean,
    defaultValue: false,
    locale: { en: "Color by data", zh: "颜色按照数据" },
    related: "gradient",
    opposite: false
  },
  overlap: {
    name: "overlap",
    type: propType.Boolean,
    defaultValue: false,
    locale: {
      en: "Overlap bars",
      zh: "重叠不同系列图表"
    }
  },
  barGap: {
    name: "barGap",
    type: propType.Percent,
    defaultValue: 0.3,
    locale: { en: "Bar gap", zh: "不同系列图表间隔" },
    related: "overlap",
    opposite: false
  },
  inverse: {
    name: "inverse",
    type: propType.Boolean,
    defaultValue: false,
    locale: {
      en: "Inverse",
      zh: "反向图表"
    }
  },
  horizontal: {
    name: "horizontal",
    type: propType.Boolean,
    defaultValue: false,
    locale: {
      en: "Horizontal",
      zh: "横向图表"
    }
  },
  useFixedBarWidth: {
    name: "useFixedBarWidth",
    type: propType.Boolean,
    locale: {
      en: "Use fixed bar width",
      zh: "使用固定宽度"
    }
  },
  barWidth: {
    name: "barWidth",
    type: propType.Number,
    defaultValue: 30,
    locale: {
      en: "bar width",
      zh: "柱形宽度"
    },
    related: "useFixedBarWidth",
    opposite: true
  },
  stack: {
    name: "stack",
    type: propType.Boolean,
    locale: {
      en: "Stack",
      zh: "堆叠图形"
    },
    defaultValue: false
  }
};

export var commonBarChartProps = [
  ...commonChartProps.content,
  propMap.dataArr,
  propMap.useFixedBarWidth,
  propMap.barWidth,
  {
    ...propMap.barCategoryGap,
    related: "useFixedBarWidth",
    opposite: false
  },
  propMap.gradient,
  propMap.colorByData,
  propMap.overlap,
  propMap.barGap,
  propMap.horizontal,
  propMap.showXAxisSplitLine,
  propMap.showYAxisSplitLine,
  propMap.showDataZoom,
  propMap.stack
];
