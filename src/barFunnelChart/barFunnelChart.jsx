/* eslint semi:1,prettier/prettier:0,one-var:0,no-unused-vars:0,yoda:0,quotes:1 */

import React from "react";
import BaseChart from "../public/baseChart.js";
import {
  getTooltip,
  getLegend,
  getToolbox,
  getVisualMap,
  getXAxis,
  getYAxis
} from "../public/barChart.js";
import arrayToChartSeries from "../public/arrayToChartSeries";

export var barFunnelProps = {
  dataArr: [["营销数据", "访问", 60],
            ["营销数据", "咨询", 40],
            ["营销数据", "订单", 20],
            ["营销数据", "点击", 80],
            ["营销数据", "展现", 100]],
            type: 'funnel',
            left: '50%',
            top: 60,
            //x2: 80,
            width: '80%',
            // height: {totalHeight} - y - y2,
            min: 0,
            max: 100,
            minSize: '0%',
            maxSize: '100%',
            sort: 'descending',
            label: {
                show: true,
                position: 'outside'
            },
            labelLine: {
                length: 10,
                lineStyle: {
                    width: 1,
                    type: 'solid'
                }
            },
            itemStyle: {
                borderWidth: 0
            },
};

export default class extends React.Component {  //React.createClass写法废弃
  constructor(props) {
    super(props); //没有super，则在子组件中无法通过this.props访问到父组件传递的值
    this.resetChart = this.resetChart.bind(this); //es6写法，确保resetChart调用时，this指代是当前组件
  }
  resetChart(enterRenderPhase) {
    const { xAxisData, series } = this.srcData();
    if (!xAxisData || !series) return;
    const option = {
      tooltip: {
                trigger: this.props.tooltipTrigger,
                formatter: "{a} <br/>{b} : {c}%"
            },
            legend: {
            },
            toolbox: {
                right: "20%",
                feature: {
                    saveAsImage: this.props.allowSaving
                        ? {
                            name: this.props.title
                        }
                        : undefined
                }
            },
            series: series
    };
    console.log("Going to enter render phase");
    enterRenderPhase(option);
  }
  srcData() {
        let chartData = this.props.dataArr;   //组件外部默认值
        let computedData = arrayToChartSeries(chartData);
        if (computedData.series) {
          return {
            ...computedData,
            series: computedData.series.map(series => {
              var s = {
                name: series.name,
                type: this.props.type,
                data: series.data.map((item, i) => {
                  var ret = {
                    name: computedData.xAxisData[i],
                    value: item
                  };
                  if (this.props.colorByData && !this.props.gradient) {
                    ret.itemStyle = {
                      color: this.props.colorArr[i % this.props.colorArr.length]
                    };
                  }
                  return ret;
                }),
                label: {
                  color: this.props.fontColor
                },
                labelLine: {
                  lineStyle: {
                    color: this.props.fontColor
                  }
                },
                barGap: this.props.overlap ? "-100%" : this.props.barGap * 100 + "%"
              };
              if (this.props.useFixedBarWidth) {
                s.barWidth = this.props.barWidth;
              } else {
                s.barCategoryGap = this.props.barCategoryGap * 100 + "%";
              }

              return s;
            })
          };
        }
        return computedData;
    }
  render() {
    return (
      <BaseChart resetChart={this.resetChart} render={this.props.render} />
    );
  }
}
